package sakamoto.util

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path}

/**
  * <pre>
  * Created on 2014/11/01
  * </pre>
  * @author K.Sakamoto
  */
object StaticValue {
   val defaultCharset = StandardCharsets.UTF_8

   val lineSeparator = System.lineSeparator

   def reader(path: Path) = {
     Files.newBufferedReader(path, defaultCharset)
   }
 }
