package sakamoto.converter

import java.io.File
import java.nio.file.Path

/**
 * <pre>
 * Created on 5/2/15.
 * </pre>
 * @author K.Sakamoto
 */
class ExtensionReviser(extension: String) {
  def revise(path: Path): Path = {
    new File(
      revise(
        path.toAbsolutePath.toString
      )
    ).toPath
  }

  def revise(fileName: String): String = {
    val period = '.'
    val tokens = fileName.split(period).init
    val builder = new StringBuilder(tokens.length * 2 + 1)
    tokens.foreach {
      s =>
        builder.
          append(s).
          append(period)
    }
    builder.append(extension).result()
  }
}
