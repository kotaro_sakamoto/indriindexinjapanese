package sakamoto.converter


import sakamoto.analyzer.Mecab
import sakamoto.text.CodePointString

import scala.collection.mutable.ListBuffer

/**
 * <pre>
 * Created on 2014/11/24
 * </pre>
 * @author K.Sakamoto
 */
class NgramSegmentator(nGram: Int, isChar: Boolean) {
  private val DELIMITER = "\u0020" //" "

  def seg(text: String, dictionaryOpt: Option[String]): String = {
    if (isChar) {
      segmentateWithCharacter(text)
    } else {
      val punctuation = "。"
      val buffer = new ListBuffer[String]()
      text.split('\n').foreach {
        line =>
          line.split(punctuation).foreach {
            sentence =>
              Mecab.parse(sentence.concat(punctuation), dictionaryOpt) foreach {
                word =>
                  if (word != "EOS") {
                    val tokens = word.split("\\t")
                    Option(tokens(
                      if (1 < tokens.size) {3} else {0}
                    )) match {
                      case Some(term) if term != "" =>
                        buffer += term
                      case otherwise =>
                    }
                  }
              }
          }
      }
      segmentateWithMorpheme(buffer.result())
    }

  }

  private def segmentate(elements: Seq[String]): Seq[Array[String]] = {
    val length = elements.length
    val elementArray: Array[String] = new Array[String](length)
    for (i <- 0 until length) {
      elementArray(i) = elements(i)
    }
    val nGramArray = ListBuffer[Array[String]]()
    for (i <- 0 to length - nGram) {
      val array = java.util.Arrays.copyOfRange(elementArray, i, i + nGram)
      nGramArray += array
    }
    nGramArray.toSeq
  }

  private def merge(segmentatedElements: Seq[Array[String]]): String = {
    val builder = new StringBuilder(calculateRequiredSize(segmentatedElements.length))
    var isFirst = true
    segmentatedElements foreach {
      segment =>
        if (isFirst) {
          segment foreach {
            builder.append
          }
          isFirst = false
        } else {
          builder.append(DELIMITER)
          segment foreach {
            builder.append
          }
        }
    }
    builder.toString()
  }

  private def calculateRequiredSize(size: Int): Int = {
    if (nGram <= size) {
      (size - nGram) * 2 + 1
    } else {
      16
    }
  }

  def segmentateWithSpaceChar(segments: Seq[String]): String = {
    val builder = new StringBuilder()
    var isFirst = true
    segments foreach {
      segment =>
        if (isFirst) {
          builder.append(segment)
          isFirst = false
        } else {
          builder.
            append(DELIMITER).
            append(segment)
        }
    }
    builder.toString()
  }

  def segmentateWithCharacter(text: String): String = {
    merge(segmentate({
      for (segment <- new CodePointString(text).toCodePoints) yield {
        new String(Array[Int](segment), 0, nGram)
      }
    }))
  }

  def segmentateWithMorpheme(morphemes: Seq[String]): String = {
    merge(segmentate(morphemes))
  }
}
