package sakamoto.converter

import java.io.{IOException, File, PrintWriter}
import java.nio.file.{Files, Path}

import sakamoto.converter.normalizer.NFKCNormalizer
import sakamoto.util.StaticValue

/**
 * <pre>
 * Created on 2/8/15.
 * </pre>
 * @author K.Sakamoto
 */
class TrecTextFileFormatReviser(nGram: Int, isChar: Boolean, dictionaryOpt: Option[String]) {
  private val regexTagHead = "^<[^>]+>".r
  private def formatTitle(title: String): String = {
    //<TITLE>{title}</TITLE>.toString
    "<TITLE>%s</TITLE>".format(title)
  }
  private def formatText(text: String): String = {
    "<TEXT>%s</TEXT>".format(text)
  }
  private val regexTitleTag = formatTitle("([^<]+)").r
  private val segmentator = new NgramSegmentator(nGram, isChar)
  private val extensionReviser = new ExtensionReviser("xml")
  private val regexTextTag = formatText("([^<]*)").r
  private val regexTextStartTag = "<TEXT>([^<]*)$".r
  private val regexTextEndTag   = "^([^<]*)</TEXT>".r

  def reviseInDirectory(inputDirectoryPath: Path, outputDirectoryPath: Path): Unit = {
    try {
      val iterator = Files.newDirectoryStream(inputDirectoryPath).iterator()

      while (iterator.hasNext) {
        revise(
          iterator.next(),
          outputDirectoryPath
        )
      }

    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }

  def revise(inputPath: Path, outputDirectoryPath: Path): Unit = {
    val reader = Files.newBufferedReader(
      inputPath,
      StaticValue.defaultCharset
    )
    val path = extensionReviser.revise(new File(new StringBuilder(3).
      append(outputDirectoryPath.toString).
      append(File.separator).
      append(inputPath.toAbsolutePath.toString.split(File.separator).last).result()).toPath)
    val writer = new PrintWriter(Files.newBufferedWriter(
      path,
      StaticValue.defaultCharset
    ))
    var lineOpt = Option(reader.readLine())
    var isText = false

    try {
      while (lineOpt.nonEmpty) {
        lineOpt match {
          case Some(line) if line != "" =>
            Option(reviseTag(line) match {
              case regexTextTag(text) =>
                formatText("\n%s\n" format segmentate(normalize(text)))
              case regexTextStartTag(text) if !isText =>
                isText = true
                "<TEXT>".concat(normalize(text))
              case regexTextEndTag(text) if isText =>
                isText = false
                normalize(text).concat("</TEXT>")
              case text if isText =>
                segmentate(normalize(text))
              case regexTitleTag(title) =>
                formatTitle(segmentate(NFKCNormalizer.normalize(title)))
              case otherwise =>
                otherwise
            }) match {
              case Some(l) if l != "" =>
                writer.println(l)
              case otherwise =>
            }
          case otherwise =>
        }
        lineOpt = Option(reader.readLine())
      }
    } catch {
      case e: IOException =>
        e.printStackTrace()
    } finally {
      try {
        writer.close()
      } catch {
        case e: IOException =>
          e.printStackTrace()
      }
    }
  }

  private def segmentate(text: String): String = {
    if (isChar) {
      segmentator.segmentateWithCharacter(text)
    } else {
      segmentator.seg(text, dictionaryOpt).concat(" ")
    }
  }

  private def reviseTag(line: String): String = {
    line.
      replaceAll("<(d|D)(o|O)(c|C)>", "<DOC>").
      replaceAll("</(d|D)(o|O)(c|C)>", "</DOC>").
      replaceAll("<(d|D)(o|O)(c|C)(n|N)(o|O)>", "<DOCNO>").
      replaceAll("</(d|D)(o|O)(c|C)(n|N)(o|O)>", "</DOCNO>").
      replaceAll("<(t|T)(e|E)(x|X)(t|T)>", "<TEXT>").
      replaceAll("</(t|T)(e|E)(x|X)(t|T)>", "</TEXT>").
      replaceAll("<(t|T)(i|I)(t|T)(l|L)(e|E)>", "<TITLE>").
      replaceAll("</(t|T)(i|I)(t|T)(l|L)(e|E)>", "</TITLE>")
  }

  private def normalize(line: String): String = {
    val builder = new StringBuilder()
    line.split("[。．]") foreach {
      case sentence if sentence != "" =>
        sentence.split("[、，]") foreach {
          case phrase if phrase != "" =>
            builder.
              append(NFKCNormalizer.normalize(phrase.trim).
                replaceAll("\u003D", "\uFF1D").//= ＝
                replaceAll("\u301C", "\uFF5E").//〜 ～
                replaceAll("\u007E", "\uFF5E")//~ ～
              ).
              append("、")
          case otherwise =>
        }
        builder.deleteCharAt(builder.size - 1).append("。")
      case otherwise =>
    }
    builder.result()
  }
}
