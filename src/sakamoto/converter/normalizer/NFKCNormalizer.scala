package sakamoto.converter.normalizer

import java.text.Normalizer

/**
 * <pre>
 * Created on 2014/11/24
 * </pre>
 * @author K.Sakamoto
 */
object NFKCNormalizer {
  private val FORM = Normalizer.Form.NFKC

  def normalize(charSequence: CharSequence): String = {
    Normalizer.normalize(charSequence, FORM)
  }

  def normalize(character: Char): Char = {
    normalize(character.toString).head
  }

  def isNormalized(charSequence: CharSequence): Boolean = {
    Normalizer.isNormalized(charSequence, FORM)
  }

  def isNormalized(character: Char): Boolean = {
    isNormalized(character.toString)
  }
}
