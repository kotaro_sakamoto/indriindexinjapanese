package sakamoto.converter.normalizer

/**
 * <pre>
 * Created on 2014/11/24
 * </pre>
 * @author K.Sakamoto
 */
class NFKCCharacter(private var character: Char) {
  character = NFKCNormalizer.normalize(character)

  def toChar: Char = {
    character
  }
}
