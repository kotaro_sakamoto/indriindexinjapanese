package sakamoto

import java.io.{File, IOException}

import sakamoto.converter.TrecTextFileFormatReviser
import sakamoto.indri.IndriIndex

/**
 * <pre>
 * Created on 2014/10/31
 * </pre>
 * @author K.Sakamoto
 */
object Main {
  def main(args : Array[String]): Unit = {
    val regexNgram          = """-(n|N)=([1-9][0-9]*)""".r
    val regexMorpheme       = """-(m|M)""".r
    val regexOutput         = """-(s|S)""".r
    val regexDictionary     = """-(d|D)""".r
    val regexIndexDirectory = """-(i|I)""".r
    val length = args.length

    var inputFilePath: Option[String] = None
    var nGram = 1
    var isMorpheme = false
    var outputDirectoryPath: Option[String] = None
    var dictionaryPath:      Option[String] = None
    var indexDirectoryPath:  Option[String] = None

    def nextArg(index: Int): Option[String] = {
      val nextIndex = index + 1
      if (nextIndex < length && !args(nextIndex).startsWith("-")) {
        Option(args(nextIndex))
      } else {
        None
      }
    }

    if (1 < length) {
      inputFilePath = Option(args.head)

      for (i <- 1 until args.length) {
        args(i) match {
          case regexNgram(s, n) =>
            val nGramOpt =
              try {
                Option(n.toInt)
              } catch {
                case e: NumberFormatException =>
                  None
              }
            nGram = nGramOpt.getOrElse(1)
            if (nGram < 1) {
              nGram = 1
            }
          case regexMorpheme(s) =>
            isMorpheme = true
          case regexOutput(s) =>
            outputDirectoryPath = nextArg(i)
          case regexDictionary(s) =>
            dictionaryPath = nextArg(i)
          case regexIndexDirectory(s) =>
            indexDirectoryPath = nextArg(i)
          case otherwise =>
        }
      }
    }

    outputDirectoryPath match {
      case Some(output) if output != "" =>
        val outputFile = new File(output)
        if (outputFile.isDirectory) {
          inputFilePath match {
            case Some(input) if input != "" =>
              val inputFile = new File(input)
              val isFile = inputFile.isFile
              if (isFile || inputFile.isDirectory) {
                val reviser = new TrecTextFileFormatReviser(nGram, !isMorpheme, dictionaryPath)
                try {
                  if (isFile) {
                    reviser.revise(inputFile.toPath, outputFile.toPath)
                  } else {
                    reviser.reviseInDirectory(inputFile.toPath, outputFile.toPath)
                  }
                } catch {
                  case e: IOException =>
                    e.printStackTrace()
                }
              }
            case otherwise =>
          }

          indexDirectoryPath match {
            case Some(indexDirectory) if indexDirectory != "" =>
              val indexDirectoryFile = new File(indexDirectory)
              if (indexDirectoryFile.isDirectory) {
                new IndriIndex(
                  outputFile.toPath,
                  indexDirectoryFile.toPath).index()
              }
            case otherwise =>
          }
        }
      case otherwise =>
    }
  }
}
