package sakamoto.indri

import java.nio.file.Path

import scala.collection.mutable.ListBuffer
import scala.sys.process.Process

/**
 * <pre>
 * Created on 2/8/15.
 * </pre>
 * @author K.Sakamoto
 */
class IndriIndex(inputPath: Path,
                 indexPath: Path) {
  def index(): Unit = {
    val buffer = ListBuffer[String]()
    Seq(
      "IndriBuildIndex",
      "-field.name=TITLE",
      "-memory=1200m",
      "-corpus.path=".concat(inputPath.toAbsolutePath.toString),
      "-corpus.class=trectext",
      "-index=".concat(indexPath.toAbsolutePath.toString)
    ).foreach {buffer.+=}

    val command = buffer.result().toSeq
    Process(command).lineStream_! foreach println
  }
}
