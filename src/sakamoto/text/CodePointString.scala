package sakamoto.text

import java.util.IllegalFormatCodePointException

/**
 * <pre>
 * Created on 2014/11/24
 * </pre>
 * @author K.Sakamoto
 */
class CodePointString(private val codePointString: Seq[Int]) extends Iterable[Int] with CharSequence {
  def this(charSequence: CharSequence) =
    this(CodePointString.toCodePoints(charSequence))

  @throws(classOf[IllegalFormatCodePointException])
  override  def toString: String = {
    new String(codePointString.toArray, 0, length())
  }

  def contentEquals(charSequence: CharSequence): Boolean = {
    toString.contentEquals(charSequence)
  }

  def contentEquals(stringBuffer: StringBuffer): Boolean = {
    toString.contentEquals(stringBuffer)
  }

  def subCodePoints(start: Int): CodePointString = {
    subCodePoints(start, length())
  }

  def subCodePoints(start: Int, end: Int): CodePointString = {
    new CodePointString(
      java.util.Arrays.copyOfRange(codePointString.toArray, start, end)
    )
  }

  def codePointAt(index: Int): String = {
    val array: Array[Int] =
      Character.toChars(codePointString(index)).map {_.toInt}
    new String(array, 0, array.length)
  }

  def getStringIndex(codePointStringIndex: Int): Int = {
    new String(codePointString.toArray, 0, codePointStringIndex).length()
  }

  def toCodePoints: Seq[Int] = {
    codePointString
  }

  override def iterator: Iterator[Int] = {
    codePointString.iterator
  }

  override def length(): Int = {
    codePointString.length
  }

  @deprecated
  override def charAt(index: Int): Char = {
    Character.toChars(codePointString(index)).head
  }

  @deprecated
  def subSequence(start: Int): CharSequence = {
    subSequence(start, length())
  }

  @deprecated
  override def subSequence(start: Int, end: Int): CharSequence = {
    subCodePoints(start, end)
  }
}

object CodePointString {
  def getCodePointStringIndex(str: String, stringIndex: Int): Int = {
    new CodePointString(str.substring(0, stringIndex)).length()
  }

  def valueOf(str: String): CodePointString = {
    new CodePointString(CodePointString.toCodePoints(str))
  }

  def valueOf(charSequence: CharSequence): CodePointString = {
    valueOf(charSequence.toString)
  }

  @throws(classOf[IllegalFormatCodePointException])
  def toString(codePointString: Int*): String = {
    toString(codePointString.toArray)
  }

  @throws(classOf[IllegalFormatCodePointException])
  def toString(codePointString: Array[Int]): String = {
    new String(codePointString, 0, codePointString.length)
  }
  def toCodePoints(charSequence: CharSequence): Array[Int] = {
    val charArray = charSequence.toString.toCharArray
    val length = charArray.length
    var surrogatePairCount = 0
    var isSkipped = false
    for (i <- 0 until length) {
      if (isSkipped) {
        isSkipped = false
      } else {
        if (0 < i && (Character isSurrogatePair (charArray(i - 1),
          charArray(i)))) {
          surrogatePairCount += 1
          isSkipped = true
        }
      }
    }
    isSkipped = false
    val codePoints = new Array[Int](length - surrogatePairCount)
    var j = 0
    for (i <- 0 until length) {
      if (isSkipped) {
        isSkipped = false
      } else {
        val currentChar = charArray(i)
        if ((Character isHighSurrogate currentChar) && i + 1 < length) {
          val nextChar = charArray(i + 1)
          if (Character isLowSurrogate nextChar) {
            codePoints(j) = Character toCodePoint (currentChar, nextChar)
            j += 1
            isSkipped = true
          }
        }
        if (!isSkipped) {
          codePoints(j) = currentChar
          j += 1
        }
      }
    }
    codePoints
  }
}
