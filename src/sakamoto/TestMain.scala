package sakamoto

/**
 * <pre>
 * Created on 5/2/15.
 * </pre>
 * @author K.Sakamoto
 */
object TestMain {
  def main(args: Array[String]): Unit = {
    Main.main(Array[String](
      "/Users/sakamoto/indriindexinjapanese/indri_index/input_trec_text/",
      "-n=1",
      "-s",
      "/Users/sakamoto/indriindexinjapanese/indri_index/corrected_trec_text/",
      "-i",
      "/Users/sakamoto/indriindexinjapanese/index"
    ))
  }
}
