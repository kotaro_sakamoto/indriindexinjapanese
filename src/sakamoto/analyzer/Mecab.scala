package sakamoto.analyzer

import java.io.File

import scala.collection.mutable.ListBuffer
import scala.sys.process.Process

/**
 *
 */
object Mecab {
  def parse(sentence: String, dictionaryOpt: Option[String]): Stream[String] = {
    val buffer = ListBuffer[String]()
    buffer += "mecab"
    dictionaryOpt match {
      case Some(dic) if dic != "" && new File(dic).canRead =>
        val dicString = new File(dic).toPath.toAbsolutePath.toString
        buffer ++= List("-d", dicString)
      case otherwise =>
    }
    (Process(Seq("echo", sentence)) #| Process(buffer.result().toSeq)).lineStream_!
  }

  def isContentWord(pos: String): Boolean = {
    Seq[String](
      "名詞",
      "形容詞",
      "動詞"
    ) foreach {
      p =>
        if (pos.startsWith(p)) {
          return true
        }
    }
    false
  }
}
