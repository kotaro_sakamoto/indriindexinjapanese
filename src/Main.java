/**
 * <pre>
 * Created on 3/22/15.
 * </pre>
 *
 * @author K.Sakamoto
 */
public class Main {
    public static void main(String args[]) {
        sakamoto.Main.main(args);
        /*
        String[] testArgs = {
                "/Users/sakamoto/indriindexinjapanese/indri_index/input_trec_text/",
                "-n=1",
                "-s",
                "/Users/sakamoto/indriindexinjapanese/indri_index/corrected_trec_text/",
                "-i",
                "/Users/sakamoto/indriindexinjapanese/index"
        };
        sakamoto.Main.main(testArgs);
        //*/
    }
}
